#pragma once

#include <QObject>

#include "kf5lockinhibit_export.h"

class LockInhibitPrivate;

class KF5LOCKINHIBIT_EXPORT LockInhibit : public QObject {

public:
    explicit LockInhibit();
    virtual ~LockInhibit();
    Q_INVOKABLE void setInhibitionOn(QString reason = QString());
    Q_INVOKABLE void setInhibitionOff();
    Q_INVOKABLE void toggleInhibition(QString reason = QString());

private:
    LockInhibitPrivate* d;
};
