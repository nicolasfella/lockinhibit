/*
    Copyright (C) 2019 Nicolas Fella <nicolas.fella@gmx.de>

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once
#include <QObject>
#include "abstractlockbackend.h"

class AndroidLockBackend : public AbstractLockBackend
{

public:
    explicit AndroidLockBackend(QObject *parent = nullptr);
    virtual ~AndroidLockBackend();

    void setInhibitionOff() override;
    void setInhibitionOn(QString reason) override;
};
