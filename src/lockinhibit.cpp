#include "lockinhibit.h"

#include "abstractlockbackend.h"

#ifdef Q_OS_ANDROID
#include "androidlockbackend.h"
#else
#include "freedesktoplockbackend.h"
#endif

#include <QDebug>

class LockInhibitPrivate {
public:
    AbstractLockBackend* m_backend = nullptr;
    bool m_inhibited;
};

LockInhibit::LockInhibit() :
    d(new LockInhibitPrivate)
{
#ifdef Q_OS_ANDROID
    d->m_backend = new AndroidLockBackend();
#else
    d->m_backend = new FreedesktopLockBackend();
#endif
}

LockInhibit::~LockInhibit()
{
    delete d;
}

void LockInhibit::setInhibitionOn(QString reason)
{
    if (!d->m_backend)
        return;

    d->m_backend->setInhibitionOn(reason);
}

void LockInhibit::setInhibitionOff()
{
    if (!d->m_backend)
        return;

    d->m_backend->setInhibitionOff();
}

void LockInhibit::toggleInhibition(QString reason)
{
    qDebug() << "FOO toggle";
    if (!d->m_backend)
        return;
    qDebug() << "FOO t2";
    if (d->m_inhibited) {
        qDebug() << "FOO oof";
        d->m_backend->setInhibitionOff();
    } else {
        qDebug() << "FOO on";
        d->m_backend->setInhibitionOn(reason);
    }

    d->m_inhibited = !d->m_inhibited;

    d->m_backend->setInhibitionOn(reason);
}
